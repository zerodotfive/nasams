#!/usr/bin/env python

import asyncio, os, subprocess, yaml, logging, hashlib
from dirhash import dirhash


WAIT_SECONDS = int(os.environ["WAIT_SECONDS"]) if "WAIT_SECONDS" in os.environ.keys() else 60
ADDONS_PATH = os.environ["ADDONS_PATH"] if "ADDONS_PATH" in os.environ.keys() else "/etc/kubernetes/nasams"
CHARTS_PATH = os.path.join(ADDONS_PATH, "charts")
VALUES_PATH = os.path.join(ADDONS_PATH, "values")
KUBECONIG_PATH = os.environ["KUBECONIG_PATH"] if "KUBECONIG_PATH" in os.environ.keys() else "/var/lib/kube-addon-manager/kubeconfig"


logger = logging.getLogger("NASAMS")


async def wait_period():
    try:
        await asyncio.sleep(WAIT_SECONDS)
    except KeyboardInterrupt:
        pass


async def helm_upgrade(addon):
    logger.info("{}/{} upgrading from {}".format(addon["namespace"], addon["name"], addon["path"]))
    if values_file(addon["namespace"], addon["name"]):
        h = subprocess.run(
            [
                "helm", "--kubeconfig", KUBECONIG_PATH,
                "upgrade", "--install", "--debug",
                "-n", addon["namespace"], "--create-namespace",
                "-f", values_file(addon["namespace"], addon["name"]),
                "--set", "chart_md5={}".format(addon["chart_md5"]),
                "--set", "values_md5={}".format(addon["values_md5"]),
                addon["name"], addon["path"]
            ],
            capture_output=True
        )
        if h.returncode:
            logger.warning("{}/{} error upgrading. Helm output: {}".format(addon["namespace"], addon["name"], h.stderr))

        return

    h = subprocess.run(
        [
            "helm", "--kubeconfig", KUBECONIG_PATH,
            "upgrade", "--install", "--debug",
            "-n", addon["namespace"], "--create-namespace",
            "--set", "chart_md5={}".format(addon["chart_md5"]),
            "--set", "values_md5={}".format(addon["values_md5"]),
            addon["name"], addon["path"]
        ],
        capture_output=True
    )
    if h.returncode:
        logger.warning("{}/{} error upgrading. Helm output: {}".format(addon["namespace"], addon["name"], h.stderr))

    return


async def get_upgradable(addon):
    h = subprocess.run(
        [
            "helm", "--kubeconfig", KUBECONIG_PATH,
            "-n", addon["namespace"],
            "status",
            addon["name"], "-o", "yaml"
        ],
        capture_output=True
    )

    if h.returncode:
        err = h.stderr.strip()
        if b"Error: release: not found" in err:
            logger.info("{}/{} chart does not exist, will be installed. Helm output: {}".format(addon["namespace"], addon["name"], err))

            return addon

        logger.warning("{}/{} error getting status. Helm output: {}".format(addon["namespace"], addon["name"], err))
        return

    addon_status = yaml.load(h.stdout, Loader=yaml.FullLoader)
    status_chart_md5 = str(addon_status["config"]["chart_md5"]) if "chart_md5" in addon_status["config"].keys() else ""
    status_values_md5 = str(addon_status["config"]["values_md5"]) if "values_md5" in addon_status["config"].keys() else ""

    if status_chart_md5 == addon["chart_md5"] and status_values_md5 == addon["values_md5"]:
        logger.info("{}/{} passed".format(addon["namespace"], addon["name"]))
        return

    logger.info("{}/{} chart has md5 mismatch: remote(chart=\"{}\" values=\"{}\") local (chart=\"{}\" values=\"{}\"), will be upgraded".format(
        addon["namespace"],
        addon["name"],
        status_chart_md5,
        status_values_md5,
        addon["chart_md5"],
        addon["values_md5"]
    ))

    return addon

def values_file(namespace, name):
    v = os.path.join(VALUES_PATH, namespace, "{}.yaml".format(name))
    if os.path.isfile(v):
        return v
    return


def list_addon_dirs(ns_path):
    for addon in os.listdir(ns_path):
        addon_path = os.path.join(ns_path, addon)
        if not os.path.isdir(addon_path):
            logger.info("{} is not a helm chart".format(addon_path))
            continue

        yield (addon, addon_path)


def list_ns_dirs():
    for ns in os.listdir(CHARTS_PATH):
        ns_path = os.path.join(CHARTS_PATH, ns)

        if not os.path.isdir(ns_path):
            logger.info("{} is not a ns dir".format(ns_path))
            continue

        yield (ns, ns_path)


def list_addons():
    for ns, ns_path in list_ns_dirs():
        for addon, addon_path in list_addon_dirs(ns_path):
            yield (ns, addon, addon_path)


def get_addons():
    for (ns, addon, addon_path) in list_addons():
        h = subprocess.run(["helm", "show", "chart", addon_path], capture_output=True)
        if h.returncode:
            logger.info("{} is not a helm chart".format(addon_path))
            continue

        values_md5 = ""
        if values_file(ns, addon):
            v = open(values_file(ns, addon), 'rb')
            values_md5 = hashlib.md5(v.read()).hexdigest()
            v.close()

        yield dict(
            name=addon,
            namespace=ns,
            path=addon_path,
            chart_md5=dirhash(addon_path, "md5"),
            values_md5=values_md5
        )


async def __main__():
    logging.basicConfig(level=logging.INFO)
    logger.info('Started')
    while True:
        addon_tasks = list(asyncio.create_task(get_upgradable(addon)) for addon in get_addons())
        addon_tasks.append(asyncio.create_task(wait_period()))
        try:
            done, pending = await asyncio.wait(
                addon_tasks,
                timeout=WAIT_SECONDS
            )
        except Exception as e:
            logger.warning("Got exception.", e)

        upgrade_tasks = list()
        for task in done:
            upgradable_addon = task.result()
            if upgradable_addon:
                upgrade_tasks.append(asyncio.create_task(helm_upgrade(upgradable_addon)))

        if len(upgrade_tasks):
            await asyncio.wait(upgrade_tasks)


asyncio.run(__main__())
