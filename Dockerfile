FROM python:slim as nasams



RUN apt update && apt install -y wget && apt clean && \
    pip install pyyaml dirhash && \
    wget https://get.helm.sh/helm-v3.14.3-linux-amd64.tar.gz && \
    tar xzf helm-v3.14.3-linux-amd64.tar.gz && \
    rm helm-v3.14.3-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/ && \
    rm -rf linux-amd64/ && \
    apt purge -y wget && \
    apt autoremove -y

ADD main.py /

CMD ["python","-u","/main.py"]
